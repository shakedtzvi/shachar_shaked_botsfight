




def do_turn(pw):
    if len(pw.my_fleets()) >= 1:
        return
    if len(pw.my_planets()) == 0:
        return
    if len(pw.neutral_planets()) >= 1:
        dest = pw.neutral_planets()[0]
    else:
        if len(pw.enemy_planets()) >= 1:
            dest = pw.enemy_planets()[0]

    source = pw.my_planets()[0]
    num_ships = source.num_ships() / 2
    myPlanets = pw.my_planets()

    while (source.num_ships() > 70):
        num_ships = 34
        for planet in myPlanets:
            if (planet.num_ships() > 100):
                pw.issue_order(source, dest, num_ships)
        if (source.num_ships() <= 40):
            break
    